FROM golang

WORKDIR /app/src

ENV GOPATH=/app

COPY . /app/src

# RUN go mod download
RUN go mod init cgomez.com
RUN go get -u github.com/gin-gonic/gin
RUN go build main.go
EXPOSE 80

CMD ["./main"]
