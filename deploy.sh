#!/bin/bash
set -eo pipefail

echo "Deploy app"

docker build -t demo . 

docker run -d -p 80:80 --name cdemo demo

