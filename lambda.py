import json
import urllib.request
from urllib.error import URLError, HTTPError
import boto3
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

SNS_TOPIC_ARN = 'arn:aws:sns:us-east-1:XXXXXXXX:notificar'

def lambda_handler(event, context):
    website_url = 'http://X.X.X.X/'
    
    sns = boto3.client('sns')
    
    try:
        response = urllib.request.urlopen(website_url)
        if response.getcode() == 200:
            logger.info('Website is up!') 
            
            sns.publish(
                TopicArn=SNS_TOPIC_ARN,
                Message='Your website is up!',
                Subject='Alert'
            )
            return {
                'statusCode': 200,
                'body': json.dumps('Website is up!')
            }
        else:
            logger.warning('Website is up, but returned a non-200 status code.')  
    except HTTPError as e:
        sns.publish(
            TopicArn=SNS_TOPIC_ARN,
            Message='Your website returned a server error: ' + str(e.code),
            Subject='Website Down Alert'
        )
        logger.error('HTTPError: ' + str(e.code))  
    except URLError as e:
        sns.publish(
            TopicArn=SNS_TOPIC_ARN,
            Message='Your website is down: ' + str(e.reason),
            Subject='Website Down Alert'
        )
        logger.error('URLError: ' + str(e.reason))  
    
    logger.error('Website is down!')
    return {
        'statusCode': 500,
        'body': json.dumps('Website is down!')
    }